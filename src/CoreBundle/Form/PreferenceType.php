<?php

namespace CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PreferenceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('discussion', null, array(
                'label' => 'Discussuion'
            ))
            ->add('cigarette', null, array(
                'label' => 'Cigarette'
            ))
            ->add('mixte', null, array(
                'label' => 'Mixte'
            ))
            ->add('musique', null, array(
                    'label' => 'Musique'
            ))
            ->add('animaux', null, array(
                'label' => 'Animaux'
            ))
            ->add('bagage', null, array(
                'label' => 'Bagage'
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\Preference'
        ));
    }
}
