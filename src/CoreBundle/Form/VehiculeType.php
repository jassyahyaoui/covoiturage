<?php

namespace CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VehiculeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('matricule', null, array(
                'label' => 'Matricule'
            ))
            ->add('marque', null, array(
                'label' => 'Marque'
            ))
            ->add('model', null, array(
                'label' => 'Modèle'
            ))
            ->add('couleur', 'choice', array(
                'label' => 'Couleur',
                'choices' => array(
                    '' => '-- Aucune --',
                    'Noir' => 'Noir',
                    'Bleu' => 'Bleu',
                    'Vert foncé' => 'Vert foncé',
                    'Bleu foncé' => 'Bleu foncé',
                    'Gris foncé' => 'Gris foncé',
                    'Violet' => 'Violet',
                    'Marron' => 'Marron',
                    'Bordeaux' => 'Bordeaux',
                    'Mauve' => 'Mauve',
                    'Rouge' => 'Rouge',
                    'Rose' => 'Rose',
                    'Orange' => 'Orange',
                    'Jaune' => 'Jaune',
                    'Blanc' => 'Blanc',
                    'Gris métallisé' => 'Gris métallisé',
                    'Beige' => 'Beige'
                )
            ))
            ->add('confort', 'choice', array(
                'label' => 'Confort',
                'choices' => array(
                    '' => '-- Aucune --',
                    'Basique' => 'Basique',
                    'Normal' => 'Normal',
                    'Confort' => 'Confort',
                    'Luxe' => 'Luxe'
                )
            ))
            ->add('nbplace', null, array(
                'label' => 'Nombre de places (y compris le chauffeur)',
                'attr' => array(
                    
                )
            ))
            ->add('categorie', 'choice', array(
                'label' => 'Catégorie',
                'choices' => array(
                    '' => '-- Aucune --',
                    'Berline' => 'Berline',
                    'Véhicule de tourisme' => 'Véhicule de tourisme',
                    'Cabriolet' => 'Cabriolet',
                    'Break' => 'Break',
                    '4x4' => '4x4',
                    'Véhicule de service' => 'Véhicule de service',
                    'Monospace' => 'Monospace',
                    'Petit utilitaire' => 'Petit utilitaire',
                    'Grand utilitaire' => 'Grand utilitaire'
                )
            ))
            ->add('climatise', null, array(
                'label' => 'Climatiseur'
            ))
            ->add('anneexperience', null, array(
                'label' => 'Années Experiences'
            ))
//            ->add('user')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\Vehicule'
        ));
    }
}
