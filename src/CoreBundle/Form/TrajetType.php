<?php

namespace CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrajetType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', null, array(
                'label' => 'Titre'
            ))
            ->add('villedepart', null, array(
                'label' => 'Ville de départ'
            ))
            ->add('villearrive', null, array(
                'label' => "Ville d'arrivée"
            ))
            ->add('datedepart', 'text', array(
                'label' => 'Date de départ'
            ))
            ->add('heuredepart', 'text', array(
                'label' => 'Heure de dépat'
            ))
            ->add('status', null, array(
                'label' => 'Statut ( affiché ou non )'
            ))
            ->add('nbplace', null, array(
                'label' => 'Places disponible'
            ))
            ->add('prix', null, array(
                'label' => 'Prix ( DT par place )'
            ))
//            ->add('user')
//            ->add('vehicule', 'entity', [
//                'empty_value' => '-- Aucune --',
//                'label' => 'Véhicule',
//                'class' => 'CoreBundle\Entity\Vehicule',
//                'property' => 'matricule',                
//                'required' => true
//            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\Trajet'
        ));
    }
}
