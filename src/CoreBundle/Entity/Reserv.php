<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reserv
 */
class Reserv
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \UserBundle\Entity\User
     */
    private $user;

    /**
     * @var \CoreBundle\Entity\Trajet
     */
    private $trajet;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     * @return Reserv
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set trajet
     *
     * @param \CoreBundle\Entity\Trajet $trajet
     * @return Reserv
     */
    public function setTrajet(\CoreBundle\Entity\Trajet $trajet = null)
    {
        $this->trajet = $trajet;

        return $this;
    }

    /**
     * Get trajet
     *
     * @return \CoreBundle\Entity\Trajet 
     */
    public function getTrajet()
    {
        return $this->trajet;
    }
    /**
     * @var boolean
     */
    private $status;


    /**
     * Set status
     *
     * @param boolean $status
     * @return Reserv
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
