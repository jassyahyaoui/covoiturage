<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trajet
 */
class Trajet
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $titre;

    /**
     * @var string
     */
    private $villedepart;

    /**
     * @var string
     */
    private $villearrive;

    /**
     * @var string
     */
    private $datedepart;

    /**
     * @var string
     */
    private $heuredepart;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var float
     */
    private $prix;

    /**
     * @var integer
     */
    private $nbplace;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $comment;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $reserv;

    /**
     * @var \UserBundle\Entity\User
     */
    private $user;

    /**
     * @var \CoreBundle\Entity\Vehicule
     */
    private $vehicule;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comment = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reserv = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Trajet
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set villedepart
     *
     * @param string $villedepart
     * @return Trajet
     */
    public function setVilledepart($villedepart)
    {
        $this->villedepart = $villedepart;

        return $this;
    }

    /**
     * Get villedepart
     *
     * @return string 
     */
    public function getVilledepart()
    {
        return $this->villedepart;
    }

    /**
     * Set villearrive
     *
     * @param string $villearrive
     * @return Trajet
     */
    public function setVillearrive($villearrive)
    {
        $this->villearrive = $villearrive;

        return $this;
    }

    /**
     * Get villearrive
     *
     * @return string 
     */
    public function getVillearrive()
    {
        return $this->villearrive;
    }

    /**
     * Set datedepart
     *
     * @param string $datedepart
     * @return Trajet
     */
    public function setDatedepart($datedepart)
    {
        $this->datedepart = $datedepart;

        return $this;
    }

    /**
     * Get datedepart
     *
     * @return string 
     */
    public function getDatedepart()
    {
        return $this->datedepart;
    }

    /**
     * Set heuredepart
     *
     * @param string $heuredepart
     * @return Trajet
     */
    public function setHeuredepart($heuredepart)
    {
        $this->heuredepart = $heuredepart;

        return $this;
    }

    /**
     * Get heuredepart
     *
     * @return string 
     */
    public function getHeuredepart()
    {
        return $this->heuredepart;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Trajet
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Trajet
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set nbplace
     *
     * @param integer $nbplace
     * @return Trajet
     */
    public function setNbplace($nbplace)
    {
        $this->nbplace = $nbplace;

        return $this;
    }

    /**
     * Get nbplace
     *
     * @return integer 
     */
    public function getNbplace()
    {
        return $this->nbplace;
    }

    /**
     * Add comment
     *
     * @param \CoreBundle\Entity\Comment $comment
     * @return Trajet
     */
    public function addComment(\CoreBundle\Entity\Comment $comment)
    {
        $this->comment[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \CoreBundle\Entity\Comment $comment
     */
    public function removeComment(\CoreBundle\Entity\Comment $comment)
    {
        $this->comment->removeElement($comment);
    }

    /**
     * Get comment
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add reserv
     *
     * @param \CoreBundle\Entity\Reserv $reserv
     * @return Trajet
     */
    public function addReserv(\CoreBundle\Entity\Reserv $reserv)
    {
        $this->reserv[] = $reserv;

        return $this;
    }

    /**
     * Remove reserv
     *
     * @param \CoreBundle\Entity\Reserv $reserv
     */
    public function removeReserv(\CoreBundle\Entity\Reserv $reserv)
    {
        $this->reserv->removeElement($reserv);
    }

    /**
     * Get reserv
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReserv()
    {
        return $this->reserv;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     * @return Trajet
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set vehicule
     *
     * @param \CoreBundle\Entity\Vehicule $vehicule
     * @return Trajet
     */
    public function setVehicule(\CoreBundle\Entity\Vehicule $vehicule = null)
    {
        $this->vehicule = $vehicule;

        return $this;
    }

    /**
     * Get vehicule
     *
     * @return \CoreBundle\Entity\Vehicule 
     */
    public function getVehicule()
    {
        return $this->vehicule;
    }
}
