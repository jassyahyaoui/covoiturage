<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Preference
 */
class Preference
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $discussion;

    /**
     * @var boolean
     */
    private $cigarette;

    /**
     * @var boolean
     */
    private $mixte;

    /**
     * @var boolean
     */
    private $musique;

    /**
     * @var boolean
     */
    private $animaux;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set discussion
     *
     * @param boolean $discussion
     * @return Preference
     */
    public function setDiscussion($discussion)
    {
        $this->discussion = $discussion;

        return $this;
    }

    /**
     * Get discussion
     *
     * @return boolean 
     */
    public function getDiscussion()
    {
        return $this->discussion;
    }

    /**
     * Set cigarette
     *
     * @param boolean $cigarette
     * @return Preference
     */
    public function setCigarette($cigarette)
    {
        $this->cigarette = $cigarette;

        return $this;
    }

    /**
     * Get cigarette
     *
     * @return boolean 
     */
    public function getCigarette()
    {
        return $this->cigarette;
    }

    /**
     * Set mixte
     *
     * @param boolean $mixte
     * @return Preference
     */
    public function setMixte($mixte)
    {
        $this->mixte = $mixte;

        return $this;
    }

    /**
     * Get mixte
     *
     * @return boolean 
     */
    public function getMixte()
    {
        return $this->mixte;
    }

    /**
     * Set musique
     *
     * @param boolean $musique
     * @return Preference
     */
    public function setMusique($musique)
    {
        $this->musique = $musique;

        return $this;
    }

    /**
     * Get musique
     *
     * @return boolean 
     */
    public function getMusique()
    {
        return $this->musique;
    }

    /**
     * Set animaux
     *
     * @param boolean $animaux
     * @return Preference
     */
    public function setAnimaux($animaux)
    {
        $this->animaux = $animaux;

        return $this;
    }

    /**
     * Get animaux
     *
     * @return boolean 
     */
    public function getAnimaux()
    {
        return $this->animaux;
    }
    /**
     * @var boolean
     */
    private $bagage;


    /**
     * Set bagage
     *
     * @param boolean $bagage
     * @return Preference
     */
    public function setBagage($bagage)
    {
        $this->bagage = $bagage;

        return $this;
    }

    /**
     * Get bagage
     *
     * @return boolean 
     */
    public function getBagage()
    {
        return $this->bagage;
    }
}
