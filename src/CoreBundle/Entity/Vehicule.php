<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vehicule
 */
class Vehicule
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $matricule;

    /**
     * @var string
     */
    private $marque;

    /**
     * @var string
     */
    private $model;

    /**
     * @var string
     */
    private $couleur;

    /**
     * @var string
     */
    private $confort;

    /**
     * @var integer
     */
    private $nbplace;

    /**
     * @var string
     */
    private $categorie;

    /**
     * @var boolean
     */
    private $climatise;

    /**
     * @var integer
     */
    private $anneeexperience;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set matricule
     *
     * @param string $matricule
     * @return Vehicule
     */
    public function setMatricule($matricule)
    {
        $this->matricule = $matricule;

        return $this;
    }

    /**
     * Get matricule
     *
     * @return string 
     */
    public function getMatricule()
    {
        return $this->matricule;
    }

    /**
     * Set marque
     *
     * @param string $marque
     * @return Vehicule
     */
    public function setMarque($marque)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get marque
     *
     * @return string 
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * Set model
     *
     * @param string $model
     * @return Vehicule
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string 
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set couleur
     *
     * @param string $couleur
     * @return Vehicule
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * Get couleur
     *
     * @return string 
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Set confort
     *
     * @param string $confort
     * @return Vehicule
     */
    public function setConfort($confort)
    {
        $this->confort = $confort;

        return $this;
    }

    /**
     * Get confort
     *
     * @return string 
     */
    public function getConfort()
    {
        return $this->confort;
    }

    /**
     * Set nbplace
     *
     * @param integer $nbplace
     * @return Vehicule
     */
    public function setNbplace($nbplace)
    {
        $this->nbplace = $nbplace;

        return $this;
    }

    /**
     * Get nbplace
     *
     * @return integer 
     */
    public function getNbplace()
    {
        return $this->nbplace;
    }

    /**
     * Set categorie
     *
     * @param string $categorie
     * @return Vehicule
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set climatise
     *
     * @param boolean $climatise
     * @return Vehicule
     */
    public function setClimatise($climatise)
    {
        $this->climatise = $climatise;

        return $this;
    }

    /**
     * Get climatise
     *
     * @return boolean 
     */
    public function getClimatise()
    {
        return $this->climatise;
    }

    /**
     * Set anneeexperience
     *
     * @param integer $anneeexperience
     * @return Vehicule
     */
    public function setAnneeexperience($anneeexperience)
    {
        $this->anneeexperience = $anneeexperience;

        return $this;
    }

    /**
     * Get anneeexperience
     *
     * @return integer 
     */
    public function getAnneeexperience()
    {
        return $this->anneeexperience;
    }
    /**
     * @var integer
     */
    private $anneexperience;


    /**
     * Set anneexperience
     *
     * @param integer $anneexperience
     * @return Vehicule
     */
    public function setAnneexperience($anneexperience)
    {
        $this->anneexperience = $anneexperience;

        return $this;
    }

    /**
     * Get anneexperience
     *
     * @return integer 
     */
    public function getAnneexperience()
    {
        return $this->anneexperience;
    }
    /**
     * @var \UserBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     * @return Vehicule
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var \CoreBundle\Entity\Trajet
     */
    private $trajet;


    /**
     * Set trajet
     *
     * @param \CoreBundle\Entity\Trajet $trajet
     * @return Vehicule
     */
    public function setTrajet(\CoreBundle\Entity\Trajet $trajet = null)
    {
        $this->trajet = $trajet;

        return $this;
    }

    /**
     * Get trajet
     *
     * @return \CoreBundle\Entity\Trajet 
     */
    public function getTrajet()
    {
        return $this->trajet;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->trajet = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add trajet
     *
     * @param \CoreBundle\Entity\Trajet $trajet
     * @return Vehicule
     */
    public function addTrajet(\CoreBundle\Entity\Trajet $trajet)
    {
        $this->trajet[] = $trajet;

        return $this;
    }

    /**
     * Remove trajet
     *
     * @param \CoreBundle\Entity\Trajet $trajet
     */
    public function removeTrajet(\CoreBundle\Entity\Trajet $trajet)
    {
        $this->trajet->removeElement($trajet);
    }
}
