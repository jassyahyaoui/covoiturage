<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use CoreBundle\Form\VehiculeType;
use CoreBundle\Entity\Vehicule;

class VehiculeController extends Controller
{
    public function indexAction(Request $request)
    {
        if(!$this->getUser()){
            return $this->redirectToRoute('app_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        
        $entity = new Vehicule();
        $vehicules = $em->getRepository('CoreBundle:Vehicule')->findBy(array('user' => $this->getUser()->getId()));
        
        $form = $this->createForm(new VehiculeType(), $entity, array(
            'action' => $this->generateUrl('app_vehicule'),
            'method' => 'POST',
        ));

        $form->handleRequest($request);
        
        if($form->isValid()){
            if($this->getUser()->getType() == 'covoitureur')
                $this->getUser()->setType('conducteur');
                        
            $entity->setUser($this->getUser());
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success', 'Enregistrement effectué avec succès.');
            return $this->redirectToRoute('app_vehicule');
        }

        
        return $this->render('AppBundle:Profile:Vehicule/editvehicule.html.twig', array(
            'form' => $form->createView(),
            'typecompte' => $this->getUser()->getType(),
            'vehicules' => $vehicules,
            'active' => 'null',
            'activenav2' => 'profil',
            'categorienav' => 'vehicule'
        ));
    }
    
    public function updateAction(Request $request, $id) {
        if(!$this->getUser()){
            return $this->redirectToRoute('app_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        $vehicule = $em->getRepository('CoreBundle:Vehicule')->find($id);
        
        $form = $this->createForm(new VehiculeType, $vehicule, array(
            'action' => $this->generateUrl('app_vehicule_edit', array('id' => $id)),
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        
        if($form->isValid()){
            $em->flush();
            $this->addFlash('success', 'Modification effectué avec succès.');
            return $this->redirectToRoute('app_vehicule');
        }
        
        return $this->render('AppBundle:Profile:Vehicule/editvehiculeupdate.html.twig', array(
            'form' => $form->createView(),
            'active' => 'null',
            'activenav2' => 'profil',
            'categorienav' => 'vehicule'
        ));
    }
    
    public function deleteAction(Request $request, $id) {
        if(!$this->getUser()){
            return $this->redirectToRoute('app_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        $vehicule = $em->getRepository('CoreBundle:Vehicule')->find($id);
        
        $trajets = $em->getRepository('CoreBundle:Trajet')->findBy(array('user' => $this->getUser()->getId(),
            'vehicule' => $id));
        dump($trajets);
        $form = $this->createFormBuilder();
        
        $form = $form->getForm();
        
        if($request->isMethod('POST')){
            $em->remove($vehicule);
            $em->flush();
            $this->addFlash('success', 'Suppression effectué avec succès.');
            return $this->redirectToRoute('app_vehicule');
        }
        
        return $this->render('AppBundle:Profile:Vehicule/editvehiculedelete.html.twig', array(
            'form' => $form->createView(),
            'id' => $id,
            'active' => 'null',
            'activenav2' => 'profil',
            'categorienav' => 'vehicule',
            'trajets' => count($trajets)
        ));
    }
        
}
