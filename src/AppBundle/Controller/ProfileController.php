<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use CoreBundle\Entity\File;
use CoreBundle\Entity\Preference;
use CoreBundle\Form\PreferenceType;
use Symfony\Component\Filesystem\Filesystem;


/**
 * Controller managing the user profile
 *
 * @author Christophe Coevoet <stof@notk.org>
 */
class ProfileController extends Controller
{
    /**
     * Show the user
     */
    public function showAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->render('AppBundle:Profile:show.html.twig', array(
            'user' => $user,
            'active' => 'null',
            'activenav2' => 'profil',
            'categorienav' => 'profil'
        ));
    }

    /**
     * Edit the user
     */
    public function editAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.profile.form.factory');

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_profile_edit');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
//            $this->addFlash('success', 'Enregistrement effectué avec succès.');
            return $response;
        }

        return $this->render('AppBundle:Profile:edit.html.twig', array(
            'form' => $form->createView(),
            'active' => 'null',
            'activenav2' => 'profil',
            'categorienav' => 'profil'
        ));
    }
    
    public function editPhotoAction(Request $request) {
        if(!$this->getUser()){
            return $this->redirectToRoute('app_homepage');
        }
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UserBundle:User')->find($this->getUser()->getID());
        
//        dump($entity->getFile());
//        exit();
        $form = $this->createFormBuilder();
        
        $form = $form->getForm();
        
        if($request->isMethod('POST')){
            $fs = new Filesystem();

            $listener = $this->get('gedmo.listener.uploadable');
            
            if($entity->getFile() != null){
                $file_ancient = $em->getRepository('CoreBundle:File')->find($entity->getFile()->getId());
            }           
            
            $file = new File();
            $listener->addEntityFileInfo($file, $_FILES['image']);
            $em->persist($file);
            
            if($entity->getFile() != null){
                if($fs->exists('../web/'.$entity->getFile()->getPath())){
                    $fs->remove('../web/'.$entity->getFile()->getPath());
                }
                $em->remove($file_ancient);
            }
            
            $entity->setFile($file);
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success', 'Photo effectué avec succès.');
        }
        
        return $this->render('AppBundle:Profile:editphoto.html.twig', array(
            'form' => $form->createView(),
            'profile' => $entity,
            'active' => 'null',
            'activenav2' => 'profil',
            'categorienav' => 'photo'
        ));
    }
    
    public function preferenceAction(Request $request) {
        if(!$this->getUser()){
            return $this->redirectToRoute('app_homepage');
        }
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CoreBundle:Preference')->find($this->getUser()->getPreference()->getId());
        
        $form = $this->createForm(new PreferenceType(), $entity, array(
            'action' => $this->generateUrl('fos_user_profile_pref'),
            'method' => 'POST',
        ));
        
        $form->handleRequest($request);
        
        if($form->isValid()){
            $em->flush();
            $this->addFlash('success', 'Enregistrement effectué avec succès.');
        }
        
        return $this->render('AppBundle:Profile:Preference/editpref.html.twig', array(
            'form' => $form->createView(),
            'active' => 'null',
            'activenav2' => 'profil',
            'categorienav' => 'pref'
        ));
    }
}
