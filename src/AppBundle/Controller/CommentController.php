<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use CoreBundle\Entity\Comment;
class CommentController extends Controller
{
    public function addAction(Request $request, $id) {
        if(!$this->getUser()){
            return $this->redirectToRoute('app_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        
        $trajet = $em->getRepository('CoreBundle:Trajet')->find($id);
        
        $com = new Comment();
        
        $com->setContent($request->request->get('comment'));
        $com->setUser($this->getUser());
        $com->setTrajet($trajet);
//        dump($com);
//        exit();
        $em->persist($com);
        $em->flush();        
        
        $this->addFlash('success', 'Commentaire ajouté avec succès.');
        return $this->redirectToRoute('app_trajet_detail', array('id' => $id));        
    }

}