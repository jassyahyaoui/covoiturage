<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('user_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        $vehicules = $em->getRepository('CoreBundle:Vehicule')->findBy(array('user' => $this->getUser()->getId()));
        
//        dump();
//        exit();
        
        return $this->render('AppBundle:Default:index.html.twig', array(
            'active' => 'homme',
            'activenav2' => 'dashboard',
            'photo' => $this->getUser()->getFile(),
            'description' => $this->getUser()->getDescription(),
            'type' => $this->getUser()->getType(),
            'vehicules' => count($vehicules)
            ));
    }
    
    public function aboutAction()
    {   
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('user_homepage');
        }
        
        return $this->render('AppBundle:Default:about.html.twig', array(
            'active' => 'about',
            'activenav2' => 'null'
            ));
    }
    
    public function contactAction(Request $request)
    {   
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('user_homepage');
        }
        
        if ($request->getMethod() == 'POST') {

            $username = $request->request->get('username');
            $email = $request->request->get('usermail');
            $subject = $request->request->get('subject');
            $msg = $request->request->get('message');

            $message = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom($email)
                    ->setTo('contact@covoiturageult.tn')
                    ->setBody($msg);

            $this->get('mailer')->send($message);

            $this->addFlash('success', 'Votre mail a été envoyer avec succès.');

            return $this->redirectToRoute('app_contact');

        }
        
        return $this->render('AppBundle:Default:contact.html.twig', array(
            'active' => 'contact',
            'activenav2' => 'null'
            ));
    }
}
