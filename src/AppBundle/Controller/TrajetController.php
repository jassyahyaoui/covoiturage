<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use CoreBundle\Entity\Trajet;
use CoreBundle\Form\TrajetType;
use CoreBundle\Entity\Reserv;

class TrajetController extends Controller
{
    public function indexAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('user_homepage');
        }
        $em = $this->getDoctrine()->getManager();
                        
        $depart = $request->query->get('depart');
        $arrivee = $request->query->get('arrivee');
        $datedepart = $request->query->get('datedepart');

//        dump($depart);
//        dump($arrivee);
//        dump($datedepart);
        
        if($datedepart == ""){
            $trajets = $em->getRepository('CoreBundle:Trajet')->findAllIndex(date('d:m:Y'), $this->getUser()->getId());
        }
        else{
            $trajets = $em->getRepository('CoreBundle:Trajet')->findAllIndexDate($datedepart, $this->getUser()->getId());
        }        
        
//        dump($trajets);
        
        return $this->render('AppBundle:Trajet:index.html.twig', array(
            'active' => 'trajet',
            'activenav2' => 'null',  
            'trajets' => $trajets,
            'depart' => $depart,
            'arrivee' => $arrivee,
            'datedepart' => $datedepart,
            'year' => date('Y')
            ));
    }
    
    public function addAction(Request $request){
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('user_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        $vehicules = $em->getRepository('CoreBundle:Vehicule')->findBy(array('user' => $this->getUser()->getId()));
        $entity = new Trajet();
        $form = $this->createForm(new TrajetType(), $entity, array(
            'action' => $this->generateUrl('app_trajet_add'),
            'method' => 'POST',
        ));
        
        $form->handleRequest($request);
        if($request->isMethod('POST')){
            $v = $em->getRepository('CoreBundle:Vehicule')->find($request->request->get('vehicule'));
            $entity->setVehicule($v);
        }
        
        if($form->isValid()){      
            $entity->setUser($this->getUser());
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success', 'Trajet Ajouter avec succès.');
            return $this->redirectToRoute('app_trajet_add');
        }
        
        return $this->render('AppBundle:Trajet:add.html.twig', array(
            'form' => $form->createView(),
            'vehicules' => $vehicules,
            'active' => 'null',
            'activenav2' => 'annonces',
            'categorienav' => 'trajet'
            ));
    }
    
    public function venirAction(Request $request){
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('user_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        
        $trajet = $em->getRepository('CoreBundle:Trajet')->findTrajetVenir(date('d-m-yy'), $this->getUser()->getId());
        
        return $this->render('AppBundle:Trajet:venir.html.twig', array(
            'trajets' => $trajet,
            'active' => 'null',
            'activenav2' => 'annonces',
            'categorienav' => 'venir'
            ));
    }
    
    public function passeAction(Request $request){
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('user_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        
        $trajet = $em->getRepository('CoreBundle:Trajet')->findTrajetPasse(date('d-m-yy'), $this->getUser()->getId());
        
        return $this->render('AppBundle:Trajet:passe.html.twig', array(
            'trajets' => $trajet,
            'active' => 'null',
            'activenav2' => 'annonces',
            'categorienav' => 'passe'
            ));
    }
    
    public function statusAction($id)
    {    
        $em = $this->getDoctrine()->getManager();

        $output_val = $em->getRepository("CoreBundle:Trajet")->switchState($id);
        return new Response($output_val);
    }
    
    public function detailAction($id){
        if(!$this->getUser()){
            return $this->redirectToRoute('app_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        
        $trajet = $em->getRepository('CoreBundle:Trajet')->find($id);
//        dump($trajet->getUser()->getFile());
//        exit();
        $reserv = $em->getRepository('CoreBundle:Reserv')->findBy(array('user' => $this->getUser()->getId(), 'trajet' => $id));
        
        if(count($reserv) == 0){
            $res = 'non';
        }
        else{
            $res = 'oui';
        }
        
        $comments = $em->getRepository('CoreBundle:Comment')->findCommentByTrajet($id);
        $reservation_accepte = $em->getRepository('CoreBundle:Reserv')->getAllTrajAccepte($id);
//        dump($comments);
//        exit();

        return $this->render('AppBundle:Trajet:detail.html.twig', array(
            'active' => 'trajet',
            'activenav2' => 'null',  
            'trajet' => $trajet,
            'year' => date('Y'),
            'reservation' => $res,
            'comments' => $comments,
            'accpet' => count($reservation_accepte)
            ));
    }
    
    public function editAction(Request $request, $id){
        if(!$this->getUser()){
            return $this->redirectToRoute('app_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        $trajet = $em->getRepository('CoreBundle:Trajet')->find($id);
        if(!$trajet || $trajet->getUser()->getId() != $this->getUser()->getId()){
            return $this->redirectToRoute('app_homepage');
        }
        
        $vehicules = $em->getRepository('CoreBundle:Vehicule')->findBy(array('user' => $this->getUser()->getId()));

        $form = $this->createForm(new TrajetType, $trajet, array(
            'action' => $this->generateUrl('app_trajet_edit', array('id' => $id)),
            'method' => 'POST',
        ));
        
        $form->handleRequest($request);
        
        if($form->isValid()){
            $em->flush();
            $this->addFlash('success', 'Modification effectué avec succès.');
            return $this->redirectToRoute('app_trajet_venir');
        }
        
        return $this->render('AppBundle:Trajet:edit.html.twig', array(
            'form' => $form->createView(),
            'vehicules' => $vehicules,
            'active' => 'null',
            'activenav2' => 'annonces',
            'categorienav' => 'null',
            'selected' => $trajet->getVehicule()->getId()
            ));
    }
    
    public function reserverAction($id) {
        if(!$this->getUser()){
            return $this->redirectToRoute('app_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        $trajet = $em->getRepository('CoreBundle:Trajet')->find($id);
        
        $reserv = new Reserv();
        $reserv->setTrajet($trajet);
        $reserv->setUser($this->getUser());
        
        $em->persist($reserv);
        $em->flush();
        
        $this->addFlash('success', 'Réservation effectué avec succès.');
        return $this->redirectToRoute('app_trajet_detail', array('id' => $id)); 
    }
    
    public function annulerAction($id) {
        if(!$this->getUser()){
            return $this->redirectToRoute('app_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        
        $reserv = $em->getRepository('CoreBundle:Reserv')->findBy(array('user' => $this->getUser()->getId(), 'trajet' => $id));
        
        $em->remove($reserv[0]);
        $em->flush();
        
        $this->addFlash('success', 'Réservation annulé avec succès.');
        return $this->redirectToRoute('app_trajet_detail', array('id' => $id)); 
    }
    
    
    
    public function indexreservationAction() {
        if(!$this->getUser()){
            return $this->redirectToRoute('app_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        
        $annonces = $em->getRepository('CoreBundle:Trajet')->findByUser($this->getUser()->getId());        
        
        $i = 0;
        while(count($annonces) > $i){
            $annonces[$i]['demandes'] = count($em->getRepository('CoreBundle:Reserv')->getAllForTraj($annonces[$i]['id']));;
            $i++;
        }
//        dump($annonces);
//        exit();
        
        return $this->render('AppBundle:Reservation:index.html.twig', array(
            'annonces' => $annonces,
            'active' => 'null',
            'activenav2' => 'reservation',
        ));
    }
    
    public function detailsreservationAction($id) {
        if(!$this->getUser()){
            return $this->redirectToRoute('app_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        
        $trajet = $em->getRepository('CoreBundle:Trajet')->findBy(array('id' => $id));
        
        $reservations = $em->getRepository('CoreBundle:Reserv')->getAllForTraj($id);
        $reservation_accepte = $em->getRepository('CoreBundle:Reserv')->getAllTrajAccepte($id);
        
//        dump($reservation_accepte);
//        exit();
        
        return $this->render('AppBundle:Reservation:details.html.twig', array(
            'reservations' => $reservations,
            'trajet' => $trajet[0],
            'active' => 'null',
            'activenav2' => 'reservation',
            'nbaccept' => count($reservation_accepte),
            'date' =>date('Y')
        ));
    }
    
    public function statusreservationAction($id)
    {    
        $em = $this->getDoctrine()->getManager();
        
        $output_val = $em->getRepository("CoreBundle:Reserv")->switchState($id);
        return new Response($output_val);
    }
    
}
