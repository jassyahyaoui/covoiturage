<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use FOS\UserBundle\Util\LegacyFormHelper;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {   
        $builder->add('username', 'text',[
            'label' => 'Login',
            'label_attr' => [
                'class' => ''
            ],
            'attr' => [
                'placeholder' => 'Login',
                'class' => ''
            ],
            'required' => false
        ]);
        $builder->add('email', 'email',[
            'label' => 'Email',
            'label_attr' => [
                'class' => ''
            ],
            'attr' => [
                'placeholder' => 'Email',
                'class' => ''
            ],
            'required' => false
        ]);
        $builder->add('lastname', 'text',[
            'label' => 'Nom',
            'label_attr' => [
                'class' => ''
            ],
            'attr' => [
                'placeholder' => 'Nom',
                'class' => ''
            ],
            'required' => false
        ]);
        $builder->add('firstname', 'text',[
            'label' => 'Prénom',
            'label_attr' => [
                'class' => ''
            ],
            'attr' => [
                'placeholder' => 'Prénom',
                'class' => ''
            ],
            'required' => false
        ]);
        $builder->add('gender', 'choice', array(
            'label' => 'Je suis',
            'label_attr' => [
                'class' => ''
            ],
            'label'=> false,
            'expanded' => true,
//            'required' => false,
            'multiple' => false,
            'choices' => array(
                'h' => 'Homme',
                'f' => 'Femme'
            ),
            'attr' => [
                
            ]
        ));
        $builder->add('born', 'choice', array(
            'label' => 'Année de naissance',
            'label_attr' => [
                'class' => ''
            ],
            'required' => false,
            'choices' => array(
                '' => '-- Année de naissance --',
                "2016" => "2016",
                "2015" => "2015",
                "2014" => "2014",
                "2013" => "2013",
                "2012" => "2012",
                "2011" => "2011",
                "2010" => "2010",
                "2009" => "2009",
                "2008" => "2008",
                "2007" => "2007",
                "2006" => "2006",
                "2005" => "2005",
                "2004" => "2004",
                "2003" => "2003",
                "2002" => "2002",
                "2001" => "2001",
                "2000" => "2000",
                "1999" => "1999",
                "1998" => "1998",
                "1997" => "1997",
                "1996" => "1996",
                "1995" => "1995",
                "1994" => "1994",
                "1993" => "1993",
                "1992" => "1992",
                "1991" => "1991",
                "1990" => "1990",
                "1989" => "1989",
                "1988" => "1988",
                "1987" => "1987",
                "1986" => "1986",
                "1985" => "1985",
                "1984" => "1984",
                "1983" => "1983",
                "1982" => "1982",
                "1981" => "1981",
                "1980" => "1980",
                "1979" => "1979",
                "1978" => "1978",
                "1977" => "1977",
                "1976" => "1976",
                "1975" => "1975",
                "1974" => "1974",
                "1973" => "1973",
                "1972" => "1972",
                "1971" => "1971",
                "1970" => "1970",
                "1969" => "1969",
                "1968" => "1968",
                "1967" => "1967",
                "1966" => "1966",
                "1965" => "1965",
                "1964" => "1964",
                "1963" => "1963",
                "1962" => "1962",
                "1961" => "1961",
                "1960" => "1960",
                "1959" => "1959",
                "1958" => "1958",
                "1957" => "1957",
                "1956" => "1956",
                "1955" => "1955",
                "1954" => "1954",
                "1953" => "1953",
                "1952" => "1952",
                "1951" => "1951",
                "1950" => "1950",
                "1949" => "1949",
                "1948" => "1948",
                "1947" => "1947",
                "1946" => "1946",
                "1945" => "1945",
                "1944" => "1944",
                "1943" => "1943",
                "1942" => "1942",
                "1941" => "1941",
                "1940" => "1940",
                "1939" => "1939",
                "1938" => "1938",
                "1937" => "1937",
                "1936" => "1936",
                "1935" => "1935",
                "1934" => "1934",
                "1933" => "1933",
                "1932" => "1932",
                "1931" => "1931",
                "1930" => "1930",
                "1929" => "1929",
                "1928" => "1928",
                "1927" => "1927",
                "1926" => "1926",
                "1925" => "1925",
                "1924" => "1924",
                "1923" => "1923",
                "1922" => "1922",
                "1921" => "1921",
                "1920" => "1920",
                "1919" => "1919",
                "1918" => "1918",
                "1917" => "1917",
                "1916" => "1916",
                "1915" => "1915",
                "1914" => "1914",
                "1913" => "1913",
                "1912" => "1912",
                "1911" => "1911",
                "1910" => "1910",
                "1909" => "1909",
                "1908" => "1908",
                "1907" => "1907",
                "1906" => "1906",
                "1905" => "1905"
            )
        ));
        $builder->add('grade', 'choice', array(
            'label' => 'Grade',
            'label_attr' => [
                'class' => ''
            ],
            'choices' => array(
                '' => '-- Grade-- ',
                'etudiant' => 'Etudiant(e)',
                'enseignant' => 'Enseignant(e)',
                'administrateur' => 'Administrateur'
            ),
            'attr' => [
                
            ],
            'required' => false
        ));
        $builder->add('telephone', 'text',[
            'label' => 'Téléphone',
            'label_attr' => [
                'class' => ''
            ],
            'attr' => [
                'placeholder' => 'Téléphone',
                'class' => ''
            ],
            'required' => false
        ]);
        $builder->add('type', 'choice', array(
            'label' => '',
            'label_attr' => [
                'class' => ''
            ],
            'expanded' => true,
            'multiple' => false,
            'choices' => array(
                'conducteur' => 'Conducteur',
                'covoitureur' => 'Covoitureur'
            ),
            'attr' => [
                
            ]
        ));
        $builder->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\RepeatedType'), array(
                'type' => LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'),
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('attr' => array('label' => 'form.password', 'placeholder' => 'Mot de passe')),
                'second_options' => array('attr' => array('label' => 'form.password', 'placeholder' => 'Entrez à nouveau le Mot de passe')),
                'invalid_message' => 'fos_user.password.mismatch',
        ))
                ->add('captcha', 'Gregwar\CaptchaBundle\Type\CaptchaType')
            ;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';

        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
