<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {   
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('app_homepage');
        }
        return $this->render('UserBundle:Default:index.html.twig', array('active' => 'homme'));
    }
    
    public function aboutAction()
    {   
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('app_homepage');
        }
        return $this->render('UserBundle:Default:about.html.twig', array('active' => 'about'));
    }
    
    public function contactAction(Request $request)
    {   
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('app_homepage');
        }
        if ($request->getMethod() == 'POST') {

            $username = $request->request->get('username');
            $email = $request->request->get('usermail');
            $subject = $request->request->get('subject');
            $msg = $request->request->get('message');

            $message = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom($email)
                    ->setTo('contact@covoiturageult.tn')
                    ->setBody($msg);

            $this->get('mailer')->send($message);

            $this->addFlash('success', 'Votre mail a été envoyer avec succès.');

            return $this->redirectToRoute('user_contact');

        }
        return $this->render('UserBundle:Default:contact.html.twig', array('active' => 'contact'));
    }
}
