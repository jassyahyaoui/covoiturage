<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
/**
 * User
 */
class User extends BaseUser
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var string
     */
    private $gender;

    /**
     * @var string
     */
    private $born;

    /**
     * @var string
     */
    private $grade;

    /**
     * @var integer
     */
    private $telephone;

    /**
     * @var string
     */
    private $type;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \CoreBundle\Entity\File
     */
    private $file;

    /**
     * @var \CoreBundle\Entity\Preference
     */
    private $preference;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $vehicule;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $trajet;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $comment;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $reserv;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->createdate = new \DateTime();
        $this->vehicule = new \Doctrine\Common\Collections\ArrayCollection();
        $this->trajet = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comment = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reserv = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set born
     *
     * @param string $born
     * @return User
     */
    public function setBorn($born)
    {
        $this->born = $born;

        return $this;
    }

    /**
     * Get born
     *
     * @return string 
     */
    public function getBorn()
    {
        return $this->born;
    }

    /**
     * Set grade
     *
     * @param string $grade
     * @return User
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return string 
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set telephone
     *
     * @param integer $telephone
     * @return User
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return integer 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return User
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return User
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set file
     *
     * @param \CoreBundle\Entity\File $file
     * @return User
     */
    public function setFile(\CoreBundle\Entity\File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return \CoreBundle\Entity\File 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set preference
     *
     * @param \CoreBundle\Entity\Preference $preference
     * @return User
     */
    public function setPreference(\CoreBundle\Entity\Preference $preference = null)
    {
        $this->preference = $preference;

        return $this;
    }

    /**
     * Get preference
     *
     * @return \CoreBundle\Entity\Preference 
     */
    public function getPreference()
    {
        return $this->preference;
    }

    /**
     * Add vehicule
     *
     * @param \CoreBundle\Entity\Vehicule $vehicule
     * @return User
     */
    public function addVehicule(\CoreBundle\Entity\Vehicule $vehicule)
    {
        $this->vehicule[] = $vehicule;

        return $this;
    }

    /**
     * Remove vehicule
     *
     * @param \CoreBundle\Entity\Vehicule $vehicule
     */
    public function removeVehicule(\CoreBundle\Entity\Vehicule $vehicule)
    {
        $this->vehicule->removeElement($vehicule);
    }

    /**
     * Get vehicule
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVehicule()
    {
        return $this->vehicule;
    }

    /**
     * Add trajet
     *
     * @param \CoreBundle\Entity\Trajet $trajet
     * @return User
     */
    public function addTrajet(\CoreBundle\Entity\Trajet $trajet)
    {
        $this->trajet[] = $trajet;

        return $this;
    }

    /**
     * Remove trajet
     *
     * @param \CoreBundle\Entity\Trajet $trajet
     */
    public function removeTrajet(\CoreBundle\Entity\Trajet $trajet)
    {
        $this->trajet->removeElement($trajet);
    }

    /**
     * Get trajet
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTrajet()
    {
        return $this->trajet;
    }

    /**
     * Add comment
     *
     * @param \CoreBundle\Entity\Comment $comment
     * @return User
     */
    public function addComment(\CoreBundle\Entity\Comment $comment)
    {
        $this->comment[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \CoreBundle\Entity\Comment $comment
     */
    public function removeComment(\CoreBundle\Entity\Comment $comment)
    {
        $this->comment->removeElement($comment);
    }

    /**
     * Get comment
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add reserv
     *
     * @param \CoreBundle\Entity\Reserv $reserv
     * @return User
     */
    public function addReserv(\CoreBundle\Entity\Reserv $reserv)
    {
        $this->reserv[] = $reserv;

        return $this;
    }

    /**
     * Remove reserv
     *
     * @param \CoreBundle\Entity\Reserv $reserv
     */
    public function removeReserv(\CoreBundle\Entity\Reserv $reserv)
    {
        $this->reserv->removeElement($reserv);
    }

    /**
     * Get reserv
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReserv()
    {
        return $this->reserv;
    }
}
